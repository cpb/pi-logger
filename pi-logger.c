/*
 * Raspberry Pi input GPIO logger.
 *
 * (c) 2018 Christophe BLAESS <christophe.blaess@logilin.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

// -------- System headers.

	#include <assert.h>
	#include <fcntl.h>
	#include <limits.h>
	#include <signal.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <time.h>


// -------- Private macros and typedefs.

	#define xstr(s) str(s)
	#define str(s) #s
	#ifndef _PI_LOGGER_VERSION
	#define _PI_LOGGER_VERSION   "unknown"
	#endif
	#define PI_LOGGER_VERSION_STR  xstr(_PI_LOGGER_VERSION)

	#define GPIO_VALUE_STRING_LENGTH   4

	#define OUTPUT_FORMAT_TABLE     0
	#define OUTPUT_FORMAT_CSV       1

	#define INVALID_GPIO_NUMBER       -1
	#define INVALID_GPIO_VALUE        -1
	#define INVALID_FILE_DESCRIPTOR   -1

	typedef struct {

		int number;
		int value;
		int file;

	} gpio_t;


	typedef struct {

		int   output_format;
		char *output_filename;
		int   wait_delay;

	} command_line_option_t;


// -------- Private global variables.

	static int    Must_quit    = 0;


// -------- Private methods declarations..

	static int       parse_command_line_options  (int argc, char *argv[], command_line_option_t *options);
	static void      display_usage_and_options   (const char *program);
	static void      display_version_and_license (const char *program);
	static gpio_t  * setup_gpios_array           (int argc, char *argv[]);
	static void      free_gpios                  (gpio_t *gpio_array);
	static gpio_t  * allocate_gpios_array        (int argc, char *argv[]);
	static int       get_gpios_numbers           (int argc, char *argv[], gpio_t *gpio_array);
	static int       export_all_input_gpios      (const char *program, gpio_t *gpio_array);
	static void      unexport_all_gpios          (gpio_t *gpio_array);
	static int       export_input_gpio           (const char *program, gpio_t *gpio);
	static void      unexport_gpio               (gpio_t *gpio);
	static int       set_gpio_wait_edges         (const char *program, gpio_t *gpio_array);
	static int       open_value_files            (const char *program, gpio_t *gpio_array);
	static void      close_value_files           (gpio_t *gpio_array);
	static void      install_signal_handlers     (void);
	static void      signal_handler              (int unused);
	static int       get_env_screen_lines        (void);
	static int       redirect_stdout             (const char *program, const char *path);
	static void      display_header              (gpio_t *gpio_array, command_line_option_t *options);
	static int       display_gpios_values        (const char *program, gpio_t *gpio_array, command_line_option_t *options);
	static int       wait_for_gpios_change       (const char *program, gpio_t *gpio_array, int delay);


// -------- Public methods implementations.

int main(int argc, char *argv[])
{
	gpio_t *gpio_array = NULL;
	command_line_option_t options;

	int nb_lines;
	int nb_screen_lines;

	if (parse_command_line_options(argc, argv, &options) != 0) {
		exit(EXIT_FAILURE);
	}

	if ((gpio_array = setup_gpios_array(argc, argv)) == NULL) {
		exit(EXIT_FAILURE);
	}

	install_signal_handlers();

	nb_screen_lines = get_env_screen_lines();

	if (options.output_filename != NULL) {
		nb_screen_lines = 0;
		if (redirect_stdout(argv[0], options.output_filename) != 0) {
			free_gpios(gpio_array);
			exit(EXIT_FAILURE);
		}
	}
	nb_lines = 0;

	while (! Must_quit) {

		if (nb_lines == 0)
			display_header(gpio_array, &options);

		if (display_gpios_values(argv[0], gpio_array, &options) != 0)
			break;

		nb_lines ++;

		if (nb_screen_lines > 1)
			nb_lines %= nb_screen_lines;

		if (wait_for_gpios_change(argv[0], gpio_array, options.wait_delay) != 0)
			break;
	}

	free_gpios(gpio_array);

	return EXIT_SUCCESS;
}


// -------- Private methods implementations.

static int parse_command_line_options(int argc, char *argv[], command_line_option_t *options)
{
	int opt;

	options->output_format = OUTPUT_FORMAT_TABLE;
	options->output_filename = NULL;
	options->wait_delay = 0;

	while ((opt = getopt(argc, argv, "cho:tvw:")) != -1) {
		switch(opt) {
			case 'c':
				options->output_format = OUTPUT_FORMAT_CSV;
				break;
			case 'h':
				display_usage_and_options(argv[0]);
				return -1;
			case 'o':
				if (strcmp(optarg, "-") != 0)
					options->output_filename = optarg;
				break;
			case 't':
				options->output_format = OUTPUT_FORMAT_TABLE;
				break;
			case 'v':
				display_version_and_license(argv[0]);
				return -1;
			case 'w':
				if ((sscanf(optarg, "%d", &(options->wait_delay)) != 1)
				 || (options->wait_delay < 0)) {
					fprintf(stderr, "%s: invalid delay '%s' for option -w.\n", argv[0], optarg);
					return -1;
				}
				break;
			default:
				fprintf(stderr, "%s: ERROR: unknown option '%c'.\n", argv[0], opt);
				return -1;
		}
	}
	if (optind == argc) {
		fprintf(stderr, "%s: ERROR: no GPIO given. (-h for help.)\n", argv[0]);
		return -1;
	}
	return 0;
}



static void display_usage_and_options(const char *program)
{
	fprintf(stderr, "usage: %s [options] gpio...\n", program);
	fprintf(stderr, "  options:\n");
	fprintf(stderr, "    -c           CSV (comma separated values) output format\n");
	fprintf(stderr, "    -h           Display this help screen and exit.\n");
	fprintf(stderr, "    -o filename  Output results in the given filen.\n");
	fprintf(stderr, "    -t           Table output format (default)\n");
	fprintf(stderr, "    -v           Display the program version number and exit.\n");
	fprintf(stderr, "    -w delay-us  Wait the amount of microseconds after each reading\n");
}



static void display_version_and_license(const char *program)
{
	fprintf(stderr, "%s - Pi Datalogger version %s.\n", program, PI_LOGGER_VERSION_STR);
	fprintf(stderr, "Copyright (c) 2018 Christophe BLAESS.\n");
	fprintf(stderr, "License GPLv2: GNU GPL version 2 <http://gnu.org/licenses/gpl.html>.\n");
	fprintf(stderr, "This is free software: you are free to change and redistribute it.\n");
	fprintf(stderr, "There is NO WARRANTY, to the extent permitted by law.\n");
}



static gpio_t *setup_gpios_array(int argc, char *argv[])
{
	gpio_t *array;

	array = allocate_gpios_array(argc, argv);
	if (array != NULL) {
		if (get_gpios_numbers(argc, argv, array) == 0) {
			if (export_all_input_gpios(argv[0], array) == 0) {
				if (set_gpio_wait_edges(argv[0], array) == 0) {
					if (open_value_files(argv[0], array) == 0)
						return array;
					close_value_files(array);
				}
				unexport_all_gpios(array);
			}
		}
		free(array);
	}
	return NULL;
}



static void free_gpios(gpio_t *gpio_array)
{
	close_value_files(gpio_array);
	unexport_all_gpios(gpio_array);
	free(gpio_array);
}



static gpio_t * allocate_gpios_array(int argc, char *argv[])
{
	int     i;
	gpio_t *array;

	assert(argc > optind);

	array = calloc(argc - optind + 1, sizeof(gpio_t));
	//                          "+ 1" to have an empty gpio struct at the end of the array.
	if (array == NULL) {
		fprintf(stderr, "%s: ERROR: not enough memory\n", argv[0]);
		return NULL;
	}
	for (i = 0; i < argc - optind + 1; i ++) {
		array[i].number = INVALID_GPIO_NUMBER;
		array[i].value  = INVALID_GPIO_VALUE;
		array[i].file   = INVALID_FILE_DESCRIPTOR;
	}

	return array;
}



static int get_gpios_numbers(int argc, char *argv[], gpio_t *gpio_array)
{
	int i;

	assert(argc > optind);
	assert(gpio_array != NULL);

	for (i = 0; i < argc - optind; i ++) {
		if (sscanf(argv[optind + i], "%d", &(gpio_array[i].number)) != 1) {
			fprintf(stderr, "%s: ERROR: not a GPIO number: %s\n", argv[0], argv[optind + i]);
			return -1;
		}
	}

	return 0;
}



static int export_all_input_gpios(const char *program, gpio_t *gpio_array)
{
	int i;

	assert(program != NULL);
	assert(gpio_array != NULL);

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++)
		if (export_input_gpio(program, &(gpio_array[i])) != 0)
			return -1;
	return 0;
}



static void unexport_all_gpios(gpio_t *gpio_array)
{
	int i;

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++)
		unexport_gpio(&(gpio_array[i]));
}



static int export_input_gpio(const char *program, gpio_t *gpio)
{
	char path[PATH_MAX];
	FILE *fp;

	strncpy(path, "/sys/class/gpio/export", PATH_MAX);

	fp = fopen(path, "w");
	if (fp == NULL) {
		fprintf(stderr, "%s: ERROR: unable to open '%s'.\n", program, path);
		return -1;
	}
	fprintf(fp, "%d\n", gpio->number);
	fclose(fp);

	snprintf(path, PATH_MAX, "/sys/class/gpio/gpio%d/direction", gpio->number);
	fp = fopen(path, "w");
	if (fp == NULL) {
		fprintf(stderr, "%s: ERROR: unable to open '%s'.\n", program, path);
		return -1;
	}
	fprintf(fp, "in\n");
	fclose(fp);

	return 0;
}



static void unexport_gpio(gpio_t *gpio)
{
	char path[PATH_MAX];
	FILE *fp;

	strncpy(path, "/sys/class/gpio/unexport", PATH_MAX);

	fp = fopen(path, "w");
	if (fp == NULL) {
		perror(path);
		return;
	}
	fprintf(fp, "%d\n", gpio->number);
	fclose(fp);
}



static int set_gpio_wait_edges(const char *program, gpio_t *gpio_array)
{
	char path[PATH_MAX];
	FILE *fp;
	int i;

	assert(program != NULL);
	assert(gpio_array != NULL);

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
		snprintf(path, PATH_MAX, "/sys/class/gpio/gpio%d/edge", gpio_array[i].number);
		fp = fopen(path, "w");
		if (fp == NULL) {
			fprintf(stderr, "%s: ERROR: unable to open '%s'.\n", program, path);
			return -1;
		}
		fprintf(fp, "both\n");
		fclose(fp);
	}

	return 0;
}



static int open_value_files(const char *program, gpio_t *gpio_array)
{
	int i;
	char path[PATH_MAX];

	assert(program != NULL);
	assert(gpio_array != NULL);

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
		snprintf(path, PATH_MAX, "/sys/class/gpio/gpio%d/value", gpio_array[i].number);
		gpio_array[i].file = open(path, O_RDONLY);
		if (gpio_array[i].file < 0) {
			fprintf(stderr, "%s: ERROR: unable to open '%s'.\n", program, path);
			return -1;
		}
	}

	return 0;
}



static void close_value_files(gpio_t *gpio_array)
{
	int i;

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
		if (gpio_array[i].file >= 0)
			close(gpio_array[i].file);
		gpio_array[i].file = INVALID_FILE_DESCRIPTOR;
	}
}



static void install_signal_handlers(void)
{
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
}



static void signal_handler(int unused)
{
	(void) unused;

	Must_quit = 1;
}



static int get_env_screen_lines(void)
{
	int result = 0;
	char *env_lines;

	if (isatty(STDOUT_FILENO)) {
		env_lines = getenv("LINES");
		if (env_lines != NULL)
			if (sscanf(env_lines, "%d", &result) != 1)
				result = 0;
		if (result < 5)
			result = 0;
	}
	return result;
}



static int redirect_stdout(const char *program, const char *path)
{
	if (freopen(path, "w", stdout) != NULL)
		return 0;
	fprintf(stderr, "%s: unable to write to %s.\n", program, path);
	return -1;
}



static void display_header(gpio_t *gpio_array, command_line_option_t *options)
{
	int i;

	if (options->output_format == OUTPUT_FORMAT_CSV) {

		fprintf(stdout, "TIME,");
		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "%2d,", gpio_array[i].number);
		}
		fprintf(stdout, "\n");

	} else {
		fprintf(stdout,"===========");

		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "=====");
		}
		fprintf(stdout, "=\n");

		fprintf(stdout, "|   TIME   ");

		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "| %2d ", gpio_array[i].number);
		}
		fprintf(stdout, "|\n");

		fprintf(stdout, "+----------");

		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "+----");
		}
		fprintf(stdout, "+\n");
	}
}



static int display_gpios_values(const char *program, gpio_t *gpio_array, command_line_option_t *options)
{
	int i;
	int value;
	int change;
	time_t now;
	struct tm * now_tm;
	char buffer[GPIO_VALUE_STRING_LENGTH];

	time(&now);
	change = 0;

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {

		if (pread(gpio_array[i].file, buffer, GPIO_VALUE_STRING_LENGTH, 0) <= 0) {
			fprintf(stderr, "%s: ERROR while reading GPIO %d value.\n", program, gpio_array[i].number);
		}
		if (sscanf(buffer, "%d", &value) == 1) {
			if (gpio_array[i].value != value) {
				gpio_array[i].value = value;
				change = 1;
			}
		}
	}

	if (! change)
		return 0;

	now_tm = localtime(&now);

	if (options->output_format == OUTPUT_FORMAT_CSV) {

		fprintf(stdout,"%02d:%02d:%02d,",
		        now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec);
		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "%d,", gpio_array[i].value);
		}
		fprintf(stdout, "\n");

	} else {

		fprintf(stdout,"| %02d:%02d:%02d ",
		        now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec);
		for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
			fprintf(stdout, "|  %d ", gpio_array[i].value);
		}
		fprintf(stdout, "|\n");
	}

	fflush(stdout);

	return 0;
}



static int wait_for_gpios_change(const char *program, gpio_t *gpio_array, int delay)
{
	int i;
	fd_set set;
	int n;

	FD_ZERO(&set);
	n = -1;

	for (i = 0; gpio_array[i].number != INVALID_GPIO_NUMBER; i ++) {
		FD_SET(gpio_array[i].file, &set);
		if (gpio_array[i].file > n)
			n = gpio_array[i].file;
	}

	if (select(n+1, NULL, NULL, &set, NULL) < 0)
		return -1;
	if (delay > 0)
		usleep(delay);

	return 0;
}


